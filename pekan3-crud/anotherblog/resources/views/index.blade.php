@extends('master')

    @section('content')
    <table class="table">
        <button type="button" class="btn btn-info" ><a href="/form">CREATE USER</a></button>
        <thead class="thead-dark">
          <tr>
            <th scope="col">NO</th>
            <th scope="col">NAMA LENGKAP</th>
            <th scope="col">EMAIL</th>
            <th scope="col">FOTO</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($tables as $key =>$table)
          <tr>
          <td> {{$key +1}} </td>
          <td> {{$table->nama_lengkap}} </td>
          <td> {{$table->email}} </td>
          <td> {{$table->foto}} </td>
          <td> 
            <a href="/show/{{$table->id}}" class="btn btn-info">SHOW</a>
            <a href="/show/{{$table->id}}/edit" class="btn btn-warning">EDIT</a>
            <a href="/show/{{$table->id}}/edit" class="btn btn-danger">DELETE</a>
          
          </td>
          </tr>
              
          @endforeach
        </tbody>
      </table>


    @endsection