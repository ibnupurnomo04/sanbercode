<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfilController extends Controller
{
    public function create(){
        return view('form');
    }

    public function store(Request $request){
       // dd($request->all());
        $request->validate([
        'nama_lengkap' => 'required'|'unique:posts',
        'email'        => 'required'
        ]);

       $query =DB::table('profil')->insert([
           "nama_lengkap"   =>$request["nama"],
           "email"          =>$request["email"],
           "foto"           =>$request["foto"]
       ]);
       return redirect('/table');
         
    }

    //CONTROLLER MENAMPILKAN TABLE
    public function index(){
        $tables= DB::table('profil')->get();

        return view('index',compact('tables'));
    }

    public function show($id){
        $showtab = DB::table('profil')->where('id', $id)->first();
        dd($showtab);
        //return view('show',compact('showtab'));
    }

    public function edit($id){
        $showtab = DB::table('profil')->where('id', $id)->first();
        return view('edit',compact('showtab'));
    }
}
