<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome() {
        return view('welcome');
        
    }

    public function welcome_post(Request $request) {

        $namadpn =$request->nama; //membuat variable request nama
        $namablkng =$request->lastnama;//membuat variable request nama belakang
        return view('welcome',compact('namadpn','namablkng'));
        
      
    }
    
    
}
